class Turtle {
  constructor(options) {
    this.setState(options);
    this.stack = [];
  }
  turnLeft() {
    this.angle -= this.rotation;
  }
  turnRight() {
    this.angle += this.rotation;
  }
  getStep() {
    return [
      this.x - this.step * sin(radians(this.angle)),
      this.y - this.step * cos(radians(this.angle)),
    ];
  }
  pushState() {
    this.stack.push(this.getState());
  }
  popState() {
    this.setState(this.stack.pop()); 
  }
  getPosition() {
    return [this.x, this.y];
  }
  getState() {
    return {
      x: this.x,
      y: this.y,
      angle: this.angle,
      rotation: this.rotation,
      step: this.step
    }; 
  }
  setState({x = this.x, y = this.y, angle = this.angle, step = this.step, rotation = this.rotation}) {
    this.x = x;
    this.y = y;
    this.angle = angle;
    this.step = step;
    this.rotation = rotation;
  }
  setPosition([x, y]) {
    this.setState({x, y});
  }
}
