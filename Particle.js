class Particle {
  constructor(x, y, angle, step, xRange, yRange) {
    this.turtle = new Turtle({
      x,
      y,
      angle,
      step,
    });
    this.xRange = xRange;
    this.yRange = yRange;
    this.h = this.getHue();
    this.s = 100;
    this.b = random(50, 180);
    this.thickness = random(1, 2);
    this.isRotated = false;
  }
  getHue() {
    const [x, y] = this.turtle.getPosition();
    const xH = ((x - this.xRange[0]) * 50) / (this.xRange[1] - this.xRange[0]);
    const yH = ((y - this.yRange[0]) * 50) / (this.yRange[1] - this.yRange[0]);
    return xH + yH;
  }
  update() {
    const seed = random();
    const threshold = this.isRotated ? 0.2 : 0.02;
    this.s = this.s - 0.5;
    if (seed < threshold) {
      this.isRotated = !this.isRotated;
      this.s = 100;
    }
    const step = this.turtle.getStep();
    colorMode(HSB, 100);
    stroke(this.getHue(), this.s, this.b);
    strokeWeight(this.thickness);
    line(...this.turtle.getPosition(), ...step);
    this.turtle.setPosition(step);
  }
  setAngle(angle) {
    if (angle) {
      this.turtle.setState({
        angle: angle + (this.isRotated ? 90 : 0),
      });
    }
  }
  getPosition() {
    return this.turtle.getPosition();
  }
}
