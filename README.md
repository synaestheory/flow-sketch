[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/synaestheory/flow-sketch)
[![Main Build](https://img.shields.io/gitlab/pipeline/synaestheory/flow-sketch/main?label=Main%20Build)](https://flow.synaestheory.online)
[![Test Build](https://img.shields.io/gitlab/pipeline/synaestheory/flow-sketch/test?label=Test%20Build)](https://flow-sketch.vercel.app)

[p5.js](https://p5js.org/reference) [flow sketch](https://flow.synaestheory.online)
