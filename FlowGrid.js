class FlowGrid {
  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.leftX = int(width * -0.5);
    this.rightX = int(width * 1.5);
    this.topY = int(height * -0.5);
    this.bottomY = int(height * 1.5);
    this.resolution = int(width * 0.056);
    this.colCount = int((this.rightX - this.leftX) / this.resolution);
    this.rowCount = int((this.bottomY - this.topY) / this.resolution);
    this.xRange = [this.leftX, this.width];
    this.yRange = [this.height / 2, this.bottomY];
    this.grid = [];
    this.particles = new Set();
    for (let x = 0; x < this.colCount; x++) {
      this.grid[x] = [];
      for (let y = 0; y < this.rowCount; y++) {
        this.grid[x][y] = random(-40, -50);
      }
    }
    this.spawnParticles();
  }
  getGridValueFromPosition(x, y) {
    const gridX = int((x - this.leftX) / this.resolution);
    const gridY = int((y - this.topY) / this.resolution);
    try {
      return this.grid[gridX][gridY];
    } catch (err) {
      // out of range don't care
    }
  }
  spawn(props = {}) {
    const x = props.x || int(random(this.xRange[0], this.xRange[1]));
    const vMin = x <= 0 ? this.yRange[0] : this.height;
    const y = props.y || int(random(vMin, this.yRange[1]));
    this.particles.add(
      new Particle(
        x,
        y,
        props.angle || this.getGridValueFromPosition(x, y) || 3.14,
        props.step || int(Math.random() * 100),
        this.xRange,
        this.yRange
      )
    );
  }
  spawnParticles(count = 50) {
    this.particles.clear();
    while (count--) {
      this.spawn();
    }
  }
  update() {
    this.particles.forEach((particle) => {
      particle.update();
      const [x, y] = particle.getPosition();
      if (x >= width || y <= 0 || x < this.leftX || y > this.bottomY) {
        this.particles.delete(particle);
        this.spawn();
      }
      particle.setAngle(this.getGridValueFromPosition(x, y) || 0);
    });
  }
}
