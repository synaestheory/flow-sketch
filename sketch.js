/// <reference path="p5.global.d.ts" />
let grid;

function setup() {
  const width =
    deviceOrientation === "portait" ? displayWidth / 2 : displayWidth;
  const height =
    deviceOrientation === "portait" ? displayHeight / 2 : displayHeight;
  createCanvas(width, height);
  background(0);
  grid = new FlowGrid(width, height);
  grid.spawnParticles();
}

function mousePressed() {
  const fs = fullscreen();
  if (fs) {
    clear();
    background(0);
    grid.spawnParticles();
  } else {
    fullscreen(!fs);
  }
}

function draw() {
  grid.update();
}
